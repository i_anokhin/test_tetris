﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameView.UI
{
    public class NoMoreMovesView : View
    {
        [SerializeField] private GameObject m_Content;
        public void Show() {
            m_Content.SetActive(true);
        }
    }
}