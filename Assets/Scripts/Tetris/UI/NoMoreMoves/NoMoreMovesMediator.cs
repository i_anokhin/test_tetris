﻿using Game;
using strange.extensions.mediation.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameView.UI
{
    public class NoMoreMovesMediator : Mediator
    {
        [Inject] public GameFinishedSignal GameFinished { get; set; }
        [Inject] public NoMoreMovesView View { get; set; }

        public override void OnRegister() {
            base.OnRegister();
            GameFinished.AddListener(HandleGameFinished);
        }

        private void HandleGameFinished() {
            View.Show();
        }
    }
}