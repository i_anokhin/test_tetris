﻿using Game;
using GameView;
using GameView.UI;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SceneStartup
{
    public class SceneContext : MVCSContext
    {
        public SceneContext(MonoBehaviour view) : base(view) {
        }

        public override IContext Start() {
            base.Start();
            injectionBinder.GetInstance<StartSignal>().Dispatch();
            return this;
        }

        protected override void addCoreComponents() {
            base.addCoreComponents();
            injectionBinder.Unbind<ICommandBinder>();
            injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
        }

        protected override void mapBindings() {
            base.mapBindings();

            commandBinder.Bind<ShapeDroppedSignal>().To<ShapeDroppedCommand>();
            commandBinder.Bind<StartSignal>().To<StartCommand>().Once();

            injectionBinder.Bind<IBoardModel>().To<BoardModel>().ToSingleton();
            injectionBinder.Bind<IShapesFactory>().To<ShapesFactory>().ToSingleton();
            injectionBinder.Bind<NextShapesModel>().To<NextShapesModel>().ToSingleton();

            injectionBinder.Bind<BoardUpdatedSignal>().To<BoardUpdatedSignal>().ToSingleton();
            injectionBinder.Bind<GameFinishedSignal>().To<GameFinishedSignal>().ToSingleton();
            injectionBinder.Bind<CellClearedSignal>().To<CellClearedSignal>().ToSingleton();
            injectionBinder.Bind<PossibleShapeAddedSignal>().To<PossibleShapeAddedSignal>().ToSingleton();
            injectionBinder.Bind<PossibleShapeRemovedSignal>().To<PossibleShapeRemovedSignal>().ToSingleton();
            injectionBinder.Bind<ShapeDropFailedSignal>().To<ShapeDropFailedSignal>().ToSingleton();

            mediationBinder.Bind<BoardView>().To<BoardMediator>();
            mediationBinder.Bind<NoMoreMovesView>().To<NoMoreMovesMediator>();
        }
    }
}