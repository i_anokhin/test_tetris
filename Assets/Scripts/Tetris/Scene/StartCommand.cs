﻿using Game;
using strange.extensions.command.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SceneStartup
{
    public class StartCommand : Command {
        [Inject] public NextShapesModel NextShapesModel {get; set;}

        public override void Execute() {
            NextShapesModel.GenerateNewShapes();
        }
    }
}
