﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SceneStartup
{
    public class SceneRoot : ContextView
    {
        private void Awake() {
            context = new SceneContext(this);
        }
    }
}