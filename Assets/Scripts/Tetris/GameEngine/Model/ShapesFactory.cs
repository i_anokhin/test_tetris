﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class ShapesFactory: IShapesFactory
    {
        private Random m_Random = new Random();
        private int m_NextId = 0;

        private List<bool[,]> m_PossibleShapes;

        public ShapesFactory() {
            m_PossibleShapes = new List<bool[,]>();
            m_PossibleShapes.Add(new bool[2, 2] { { true, true }, { true, true } });
            m_PossibleShapes.Add(new bool[1, 4] { { true, true, true, true } });
            m_PossibleShapes.Add(new bool[4, 1] { { true }, { true }, { true }, { true } });
            m_PossibleShapes.Add(new bool[2, 3] { { true, true, false }, { false, true, true } });
            m_PossibleShapes.Add(new bool[2, 3] { { false, true, true }, { true, true, false } });
            m_PossibleShapes.Add(new bool[3, 2] { { false, true }, { true, true }, { true, false } });
            m_PossibleShapes.Add(new bool[3, 2] { { true, false }, { true, true }, { false, true } });
            m_PossibleShapes.Add(new bool[2, 3] { { true, false, false }, { true, true, true } });
            m_PossibleShapes.Add(new bool[2, 3] { { true, true, true } , { true, false, false } });
            m_PossibleShapes.Add(new bool[3, 2] { { true, true }, { true, false }, { true, false } });
            m_PossibleShapes.Add(new bool[3, 2] { { false, true }, { false, true }, { true, true } });
        }

        public Shape GetNextShape() {
            return new Shape(m_NextId++, m_PossibleShapes[m_Random.Next(m_PossibleShapes.Count)]);
        }
    }
}
