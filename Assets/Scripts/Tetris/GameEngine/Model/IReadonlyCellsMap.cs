﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public interface IReadonlyCellsMap
    {
        bool this[int x, int y] { get; }
        int Width { get; }
        int Height { get; }
    }
}
