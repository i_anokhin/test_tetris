﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class CellsMap : IReadonlyCellsMap
    {
        private bool[,] m_Cells = new bool[10, 10];

        public CellsMap(int width, int height) {
            if (width <= 0 || height <= 0) {
                throw new ArgumentException("Width and height must be greater than 0");
            }
            m_Cells = new bool[width, height];
        }

        public bool this[int x, int y] => m_Cells[x,y];
        public int Width => m_Cells.GetLength(0);
        public int Height => m_Cells.GetLength(1);
        public void SetCell(int x, int y, bool value) {
            m_Cells[x, y] = value;
        }
    }
}
