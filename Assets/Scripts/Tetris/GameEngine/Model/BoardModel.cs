﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public class BoardModel: IBoardModel
    {
        [Inject] public BoardUpdatedSignal BoardUpdated { get; set; }
        [Inject] public CellClearedSignal CellsCleared { get; set; }
        private CellsMap m_Cells;

        public IReadonlyCellsMap GetCells() {
            return m_Cells;
        }

        public BoardModel() {
            m_Cells = new CellsMap(10, 10);
        }

        public bool TryAddCells(IReadonlyCellsMap map, Vector2Int position) {
            if (!ShapeFitsInBoard(map, position)) {
                return false;
            }
            if (ShapeIntersectsWithFilledCells(map, position)) {
                return false;
            }
            FillCellsByShape(map, position);
            return true;
        }

        public bool HasPlaceForShape(IReadonlyCellsMap map) {
            for (var i = m_Cells.Width - map.Width; i >= 0; i--) {
                for (var j = m_Cells.Height - map.Height; j >= 0; j--) {
                    if (!ShapeIntersectsWithFilledCells(map, new Vector2Int(i,j))) {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ShapeFitsInBoard(IReadonlyCellsMap map, Vector2Int position) {
            if (position.x < 0 || position.y < 0) {
                return false;
            }
            var endCell = new Vector2Int(position.x + map.Width, position.y + map.Height);
            if (endCell.x > m_Cells.Width || endCell.y > m_Cells.Height) {
                return false;
            }
            return true;
        }

        public bool ShapeIntersectsWithFilledCells(IReadonlyCellsMap map, Vector2Int position) {
            if (!ShapeFitsInBoard(map, position)) {
                throw new ArgumentException("Shape is out of board");
            }
            for (var i = map.Width - 1; i >= 0; i--) {
                for (var j = map.Height - 1; j >= 0; j--) {
                    if (m_Cells[i + position.x, j + position.y] && map[i, j]) {
                        return true;
                    }
                }
            }
            return false;
        }

        
        public void ClearFilledRowsAndColumns() {
            var result = GetCellsToClear();
            result.ForEach(pos => {
                if (m_Cells[pos.x, pos.y]) {
                    m_Cells.SetCell(pos.x, pos.y, false);
                    CellsCleared?.Dispatch(pos);
                }
            });
        }

        private List<Vector2Int> GetCellsToClear() {
            var result = new List<Vector2Int>();
            for (var i = m_Cells.Width - 1; i >= 0; i--) {
                if (IsColumnFilled(i)) {
                    for (var j = m_Cells.Height - 1; j >= 0; j--) {
                        result.Add(new Vector2Int(i, j));
                    }
                }
            }
            for (var j = m_Cells.Height - 1; j >= 0; j--) {
                if (IsRowFilled(j)) {
                    for (var i = m_Cells.Width - 1; i >= 0; i--) {
                        result.Add(new Vector2Int(i, j));
                    }
                }
            }

            return result;
        }

        private bool IsRowFilled(int row) {
            for (var i = m_Cells.Width - 1; i >= 0; i--) {
                if (!m_Cells[i, row]) {
                    return false;
                }
            }
            return true;
        }

        private bool IsColumnFilled(int column) {
            for (var i = m_Cells.Height - 1; i >= 0; i--) {
                if (!m_Cells[column, i]) {
                    return false;
                }
            }
            return true;
        }

        private void FillCellsByShape(IReadonlyCellsMap map, Vector2Int position) {
            for (var i = map.Width - 1; i >= 0; i--) {
                for (var j = map.Height - 1; j >= 0; j--) {
                    if (map[i, j]) {
                        m_Cells.SetCell(i + position.x, j + position.y, true);
                    }
                }
            }
            BoardUpdated?.Dispatch();
        }

    }
}
