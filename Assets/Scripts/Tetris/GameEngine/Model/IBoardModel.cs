﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public interface IBoardModel
    {
        IReadonlyCellsMap GetCells();
        bool TryAddCells(IReadonlyCellsMap map, Vector2Int position);
        bool HasPlaceForShape(IReadonlyCellsMap cells);
        void ClearFilledRowsAndColumns();
    }
}
