﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Shape
    {
        private CellsMap m_Cells;

        public IReadonlyCellsMap Cells => m_Cells; 
        public int Id { get; private set;}
        public Shape(int id, bool[,] cells) {
            Id = id;
            m_Cells = new CellsMap(cells.GetLength(0), cells.GetLength(1));
            for (var i = cells.GetUpperBound(0); i >= 0; i--) {
                for (var j = cells.GetUpperBound(1); j>= 0; j--) {
                    m_Cells.SetCell(i, j, cells[i,j]);
                }
            }
        }
    }
}
