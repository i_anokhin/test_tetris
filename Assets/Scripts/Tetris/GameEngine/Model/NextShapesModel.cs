﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class NextShapesModel
    {
        [Inject] public IShapesFactory ShapesFactory { get; set;}
        [Inject] public PossibleShapeAddedSignal ShapeAdded { get; set;}
        [Inject] public PossibleShapeRemovedSignal ShapeRemoved { get; set;}

        private List<Shape> m_Shapes = new List<Shape>();
        public void GenerateNewShapes() {
            while (m_Shapes.Count < 3) {
                AddShape();
            }
        }

        public IReadOnlyList<Shape> Shapes => m_Shapes;

        private void AddShape() {
            var shape = ShapesFactory.GetNextShape();
            m_Shapes.Add(shape);
            ShapeAdded.Dispatch(shape);
        }

        public void Remove(int shapeId) {
            var shape = m_Shapes.Find(sh => sh.Id == shapeId);
            if (shape != null) {
                m_Shapes.Remove(shape);
                ShapeRemoved.Dispatch(shape);
            }
        }
    }
}
