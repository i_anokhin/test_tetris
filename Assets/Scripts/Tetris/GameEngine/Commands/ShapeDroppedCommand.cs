﻿using strange.extensions.command.impl;
using System.Linq;
using UnityEngine;

namespace Game
{
    public class ShapeDroppedCommand : Command
    {
        [Inject] public Shape Shape { get; set;}
        [Inject] public Vector3Int DropPosition { get; set;}
        [Inject] public IBoardModel Board { get; set;}
        [Inject] public NextShapesModel NextShapes { get; set;}
        [Inject] public GameFinishedSignal GameFinished { get; set;}
        [Inject] public ShapeDropFailedSignal DropFailed { get; set;}
        public override void Execute() {
            if (Board.TryAddCells(Shape.Cells, new Vector2Int(DropPosition.x, DropPosition.y))) {
                Board.ClearFilledRowsAndColumns();
                NextShapes.Remove(Shape.Id);
                var nextShapes = NextShapes.Shapes;
                if (nextShapes.Count == 0) {
                    NextShapes.GenerateNewShapes();
                    nextShapes = NextShapes.Shapes;
                }
                var possibleMove = nextShapes.Count(s => Board.HasPlaceForShape(s.Cells));
                if (possibleMove == 0) {
                    GameFinished.Dispatch();
                }

            } else {
                DropFailed.Dispatch(Shape);
            }
        }
    }
}
