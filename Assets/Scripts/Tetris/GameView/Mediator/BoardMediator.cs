﻿using Game;
using strange.extensions.mediation.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameView
{
    public class BoardMediator : Mediator
    {

        [Inject] public PossibleShapeAddedSignal ShapeAdded { get; set; }
        [Inject] public PossibleShapeRemovedSignal ShapeRemoved { get; set; }
        [Inject] public BoardUpdatedSignal BoardUpdated { get; set; }
        [Inject] public CellClearedSignal CellCleared { get; set; }
        [Inject] public ShapeDroppedSignal ShapeDropped { get; set; }
        [Inject] public ShapeDropFailedSignal ShapeDropFailed { get; set; }
        [Inject] public IBoardModel BoardModel { get; set; }
        [Inject] public BoardView View { get; set; }

        public override void OnRegister() {
            base.OnRegister();
            ShapeAdded.AddListener(HandleShapeAdded);
            ShapeRemoved.AddListener(HandleShapeRemoved);
            CellCleared.AddListener(HandleCellCleared);
            BoardUpdated.AddListener(HandleBoardUpdated);
            View.ShapeDropped.AddListener(HandleShapeDropped);
            ShapeDropFailed.AddListener(HandleShapeDropFailed);
        }

        private void HandleShapeDropFailed(Shape obj) {
            View.ResetSwippedShape();
        }

        private void HandleShapeDropped(Shape shape, Vector3Int position) {
            ShapeDropped.Dispatch(shape, position);
        }

        private void HandleBoardUpdated() {
            View.UpdateBoard(BoardModel.GetCells());
        }

        private void HandleCellCleared(Vector2Int obj) {
            View.RemoveCellAtPosition(obj);
        }

        private void HandleShapeRemoved(Shape obj) {
            View.RemovePossibleShape(obj);
        }

        private void HandleShapeAdded(Shape obj) {
            View.AddPossibleShape(obj);
        }
    }
}