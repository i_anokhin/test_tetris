﻿using Utils;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using Game;

namespace GameView
{
    public class BoardView : View
    {
        [SerializeField] private Grid m_Grid;
        [SerializeField] private Tilemap m_TileMap;
        [SerializeField] private TileBase m_TileSprite;
        [SerializeField] private TileRemoveAnimation m_RemoveAnimation;
        [SerializeField] private Transform[] m_NextShapesAnchors;
        [SerializeField] private Transform m_NextShapesSpawnPosition;
        [SerializeField] private ShapesViewPool m_ShapesPool;
        [SerializeField] private InputEventProvider m_InputEventProvider;

        public Signal<Shape, Vector3Int> ShapeDropped { get; internal set; } = new Signal<Shape, Vector3Int>();
        private List<NextShapeView> m_NextShapes = new List<NextShapeView>();
        private NextShapeView m_SwipedShape;

        protected override void Start() {
            m_InputEventProvider.BeginDrag.AddListener(HandleBeginDrag);
            m_InputEventProvider.Drag.AddListener(HandleDrag);
            m_InputEventProvider.EndDrag.AddListener(HandleEndDrag);
        }

        private void HandleDrag(PointerEventData eventData) {
            if (m_SwipedShape == null) {
                return;
            }
            var dragDelta = CameraUtil.ScreenDeltaToWorldDelta(eventData.pressEventCamera, eventData.delta);
            m_SwipedShape.MoveBy(dragDelta);
        }

        private void HandleEndDrag(PointerEventData obj) {
            if (m_SwipedShape == null) {
                return;
            }
            ShapeDropped.Dispatch(m_SwipedShape.Shape, m_Grid.WorldToCell(m_SwipedShape.GetFirstCellWorldPosition()));
        }

        private void HandleBeginDrag(PointerEventData eventData) {
            if (m_SwipedShape != null) {
                return;
            }
            var collider = Physics2D.OverlapPoint(eventData.pointerPressRaycast.worldPosition);
            m_SwipedShape = collider?.GetComponent<NextShapeView>();
        }

        public void SetTiles(IReadonlyCellsMap map) {
            for (var i = map.Width - 1; i >= 0; i--) {
                for (var j = map.Height - 1; j >= 0; j--) {
                    m_TileMap.SetTile(new Vector3Int(i, j, 0), map[i, j] ? m_TileSprite : null);
                }
            }
        }

        public void AddPossibleShape(Shape shape) {

            var holder = GetEmptyHolder();
            if (holder != null) {
                var shapeView = m_ShapesPool.Get(holder);
                shapeView.transform.position = m_NextShapesSpawnPosition.position;
                shapeView.SetHolder(holder.position);
                shapeView.SetShape(shape);
                shapeView.ResetPositionAnimated();
                m_NextShapes.Add(shapeView);
            }
        }

        private void HandleShapeBeginDrag(NextShapeView shapeView) {
            m_SwipedShape = shapeView;
        }

        public void ResetSwippedShape() {
            if (m_SwipedShape != null) {
                m_SwipedShape.ResetPositionAnimated();
                m_SwipedShape = null;
            }
        }

        private void HandleShapeDropped(NextShapeView obj) {
            ShapeDropped.Dispatch(obj.Shape, m_Grid.WorldToCell(obj.GetFirstCellWorldPosition()));
        }

        private Transform GetEmptyHolder() {
            for (var i = 0; i < m_NextShapesAnchors.Length; i++) {
                if (m_NextShapesAnchors[i].childCount == 0) {
                    return m_NextShapesAnchors[i];
                }
            }
            return null;
        }

        public void RemovePossibleShape(Shape shape) {
            var shapeView = m_NextShapes.Find(view => view.Shape == shape);
            if (shapeView != null) {
                m_ShapesPool.Release(shapeView);
                if (m_SwipedShape == shapeView) {
                    m_SwipedShape = null;
                }
            }
        }

        public void RemoveCellAtPosition(Vector2Int obj) {
            var cellPosition = new Vector3Int(obj.x, obj.y, 0);
            var cellWorldPosition = m_TileMap.GetCellCenterWorld(cellPosition);
            cellWorldPosition.z = -0.2f;
            var removeAnimation = Instantiate(m_RemoveAnimation, transform, false);
            removeAnimation.transform.position = cellWorldPosition;
            removeAnimation.Animate();
            m_TileMap.SetTile(cellPosition, null);
        }

        public void UpdateBoard(IReadonlyCellsMap cells) {
            SetTiles(cells);
        }
    }
}