﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using strange.extensions.signal.impl;
using System;
using System.Collections.Generic;
using Game;

namespace GameView
{
    public class NextShapeView : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer m_TileOrigin;
        [SerializeField] private Transform m_TileContainer;

        public Vector3 TilesOffset { get; private set; }
        private Vector3 m_HolderAnchor;
        public Shape Shape { get; private set; }

        private List<SpriteRenderer> m_Tiles = new List<SpriteRenderer>();
        private Vector3 m_FirstCellOffset;
        public void SetShape(Shape shape) {
            Shape = shape;
            var cells = shape.Cells;
            var tileIndex = 0;
            for (var i = 0; i < cells.Width; i++) {
                for (var j = 0; j < cells.Height; j++) {
                    if (cells[i, j]) {
                        if (tileIndex >= m_Tiles.Count) {
                            m_Tiles.Add(Instantiate(m_TileOrigin, m_TileContainer, false));
                        }
                        var tile = m_Tiles[tileIndex];
                        tile.gameObject.SetActive(true);
                        tile.transform.localPosition = new Vector3(i, j);
                        tileIndex++;
                    }
                }
            }
            for (var i = tileIndex; i < m_Tiles.Count; i++) {
                m_Tiles[tileIndex].gameObject.SetActive(false);
            }
            TilesOffset = new Vector3(-(cells.Width - 1) / 2f, -(cells.Height - 1) / 2f);
            m_TileContainer.localPosition = TilesOffset;
        }

        public void SetHolder(Vector3 position) {
            m_HolderAnchor = position;
        }

        public void ResetPositionAnimated() {
            if (GetComponent<ResetPositionAnimator>() == null) {
                gameObject.AddComponent<ResetPositionAnimator>();
            }
        }

        private IEnumerator ResetPositionAnimatedRoutine() {
            var targetVector = transform.localPosition;

            for (var i = 0; i < 60; i++) {
                transform.localPosition -= targetVector / 60;
                yield return null;
            }
            transform.localPosition = Vector3.zero;
            yield return null;
        }

        public void MoveBy(Vector3 offset) {
            transform.position += offset;
        }

        public Vector3 GetFirstCellWorldPosition() {
            return transform.position + TilesOffset * transform.lossyScale.x;
        }

    }
}