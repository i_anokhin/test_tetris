﻿using UnityEngine;
using System.Collections;
namespace GameView
{
    public class ResetPositionAnimator : MonoBehaviour
    {
        private const float c_UnitsPerSecond = 10f;
        private IEnumerator Start() {
            while (transform.localPosition.magnitude > c_UnitsPerSecond * Time.deltaTime) {
                transform.localPosition -= transform.localPosition.normalized * c_UnitsPerSecond * Time.deltaTime;
                yield return null;
            }
            yield return null;
            transform.localPosition = Vector3.zero;
            Destroy(this);
        }

    }
}