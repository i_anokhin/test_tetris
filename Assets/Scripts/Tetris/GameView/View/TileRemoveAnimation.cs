﻿using UnityEngine;
using System.Collections;
using System;
namespace GameView
{
    public class TileRemoveAnimation : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer m_Block;
        public void Animate() {
            StartCoroutine(HidingAnimation());
        }

        private IEnumerator HidingAnimation() {
            var alphaDecrement = 1 / 60f;
            var scaleDecrement = m_Block.transform.localScale / 60f;
            for (var i = 0; i < 60; i++) {
                var color = m_Block.color;
                color.a -= alphaDecrement;
                m_Block.color = color;
                m_Block.transform.localScale += scaleDecrement;
                yield return null;
            }
            Destroy(gameObject);
        }
    }
}
