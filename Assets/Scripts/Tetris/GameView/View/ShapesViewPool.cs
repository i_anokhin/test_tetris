﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameView
{
    public class ShapesViewPool : MonoBehaviour
    {
        [SerializeField] private NextShapeView m_NextShapesOrigin;

        public NextShapeView Get(Transform parent) {
            NextShapeView result = null;
            for (var i = transform.childCount - 1; i >= 0; i--) {
                var child = transform.GetChild(i);
                result = child.GetComponent<NextShapeView>();
                if (result == null) {
                    Destroy(child.gameObject);
                }
                else {
                    result.gameObject.transform.SetParent(parent);
                    result.gameObject.SetActive(true);
                    break;
                }
            }
            if (result == null) {
                result = Instantiate(m_NextShapesOrigin, parent, false);
            }
            return result;
        }
        public void Release(NextShapeView shapeView) {
            shapeView.gameObject.SetActive(false);
            shapeView.gameObject.transform.SetParent(transform);
        }
    }
}