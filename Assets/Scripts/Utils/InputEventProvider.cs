﻿using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputEventProvider : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Signal<PointerEventData> PointerDown { get; private set;} = new Signal<PointerEventData>();
    public Signal<PointerEventData> BeginDrag { get; private set;} = new Signal<PointerEventData>();
    public Signal<PointerEventData> Drag { get; private set;} = new Signal<PointerEventData>();
    public Signal<PointerEventData> EndDrag { get; private set;} = new Signal<PointerEventData>();

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        BeginDrag.Dispatch(eventData);
    }

    void IDragHandler.OnDrag(PointerEventData eventData) {
        Drag.Dispatch(eventData);
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData) {
        EndDrag.Dispatch(eventData);
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {
        PointerDown.Dispatch(eventData);
    }
}
