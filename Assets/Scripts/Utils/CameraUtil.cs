﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Utils
{
    public static class CameraUtil
    {
        public static Vector3 ScreenDeltaToWorldDelta(Camera camera, Vector3 delta) {
            var worldDeltaBase = camera.ScreenToWorldPoint(Vector2.zero);
            var worldDelta = camera.ScreenToWorldPoint(delta);
            return worldDelta - worldDeltaBase;
        }
    }
}
