﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleToViewport : MonoBehaviour
{
    [SerializeField] private Camera m_Camera;
    [SerializeField] private SpriteRenderer m_ReferenceSprite;

    private void Start() {
        var referenceRatio = m_ReferenceSprite.size.x / m_ReferenceSprite.size.y;
        if (referenceRatio > m_Camera.aspect) {
            transform.localScale = Vector3.one / referenceRatio * m_Camera.aspect;
        }
        Destroy(this);
    }
}
