﻿using Game;
using NUnit.Framework;

namespace Tests
{
    public class BoardTests
    {
        [Test]
        public void Test_ShapeFitsInBoard() {
            var board = new BoardModel();
            var shape = new Shape(0, new bool[3, 3] { { true, true, true }, { true, true, true }, { true, true, true } });
            Assert.IsTrue(board.ShapeFitsInBoard(shape.Cells, new UnityEngine.Vector2Int(0, 0)));
            Assert.IsFalse(board.ShapeFitsInBoard(shape.Cells, new UnityEngine.Vector2Int(-1, 0)));
            Assert.IsFalse(board.ShapeFitsInBoard(shape.Cells, new UnityEngine.Vector2Int(0, -1)));
            Assert.IsFalse(board.ShapeFitsInBoard(shape.Cells, new UnityEngine.Vector2Int(9, 0)));
            Assert.IsFalse(board.ShapeFitsInBoard(shape.Cells, new UnityEngine.Vector2Int(0, 9)));
        }

        [Test]
        public void Test_AddShapesToBoard() {
            var board = new BoardModel();
            var shape = new Shape(0, new bool[1, 2] { { true, true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(2, 1));
            Assert.IsTrue(board.GetCells()[2, 1]);
            Assert.IsTrue(board.GetCells()[2, 2]);
            shape = new Shape(0, new bool[3, 3] { { true, true, true }, { true, false, false }, { true, false, false } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(0, 0));
            Assert.IsTrue(board.GetCells()[0, 0]);
            Assert.IsTrue(board.GetCells()[0, 1]);
            Assert.IsTrue(board.GetCells()[0, 2]);
            Assert.IsTrue(board.GetCells()[1, 0]);
            Assert.IsFalse(board.GetCells()[1, 1]);
            Assert.IsFalse(board.GetCells()[1, 2]);
            Assert.IsTrue(board.GetCells()[2, 0]);
            Assert.IsTrue(board.GetCells()[2, 1]);
            Assert.IsTrue(board.GetCells()[2, 2]);
        }

        [Test]
        public void Test_ShapeIntersectsWithCells() {
            var board = new BoardModel();
            var shape = new Shape(0, new bool[2, 2] { { true, true }, { true, true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(0, 0));
            Assert.IsTrue(board.ShapeIntersectsWithFilledCells(shape.Cells, new UnityEngine.Vector2Int(0, 0)));
            Assert.IsTrue(board.ShapeIntersectsWithFilledCells(shape.Cells, new UnityEngine.Vector2Int(1, 0)));
            Assert.IsFalse(board.ShapeIntersectsWithFilledCells(shape.Cells, new UnityEngine.Vector2Int(2, 0)));
            Assert.IsTrue(board.ShapeIntersectsWithFilledCells(shape.Cells, new UnityEngine.Vector2Int(0, 1)));
            Assert.IsTrue(board.ShapeIntersectsWithFilledCells(shape.Cells, new UnityEngine.Vector2Int(1, 1)));
            Assert.IsFalse(board.ShapeIntersectsWithFilledCells(shape.Cells, new UnityEngine.Vector2Int(0, 2)));

        }

        [Test]
        public void Test_ClearFilledColumn() {
            var board = new BoardModel();
            var shape = new Shape(0, new bool[1, 10] { { true, true, true, true, true, true, true, true, true, true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(0, 0));
            shape = new Shape(0, new bool[1, 9] { { true, true, true, true, true, true, true, true, true} });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(1, 0));
            Assert.IsTrue(board.GetCells()[0, 0]);
            Assert.IsTrue(board.GetCells()[1, 0]);
            board.ClearFilledRowsAndColumns();
            Assert.IsFalse(board.GetCells()[0, 0]);
            Assert.IsTrue(board.GetCells()[1, 0]);
        }

        [Test]
        public void Test_ClearFilledRow() {
            var board = new BoardModel();
            var shape = new Shape(0, new bool[10, 1] { { true },{ true }, { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(0, 0));
            shape = new Shape(0, new bool[9, 1] { { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(0, 1));
            Assert.IsTrue(board.GetCells()[0, 0]);
            Assert.IsTrue(board.GetCells()[1, 0]);
            board.ClearFilledRowsAndColumns();
            Assert.IsFalse(board.GetCells()[0, 0]);
            Assert.IsTrue(board.GetCells()[0, 1]);
        }

        [Test]
        public void Test_HasPlaceForShape() {
            var board = new BoardModel();
            var shape = new Shape(0, new bool[10, 1] { { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(0, 4));
            shape = new Shape(0, new bool[1,4 ] { { true, true, true, true } });
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(4, 0));
            board.TryAddCells(shape.Cells, new UnityEngine.Vector2Int(4, 5));

            shape = new Shape(0, new bool[10, 1] { { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true }, { true } });
            Assert.IsTrue(board.HasPlaceForShape(shape.Cells));
            shape = new Shape(0, new bool[1, 10] { { true, true, true, true, true, true, true, true, true, true } });
            Assert.IsFalse(board.HasPlaceForShape(shape.Cells));
        }
    }
}
